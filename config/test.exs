use Mix.Config

# Configure your database
config :neo_kitten, NeoKitten.Repo,
  username: "postgres",
  password: "postgres",
  database: "neo_kitten_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :neo_kitten, NeoKittenWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn
