defmodule NeoKitten.CatApiClient do

    # todo probably better error handling than just let it fail
    def get() do
        response = HTTPotion.get("https://api.thecatapi.com/v1/images/search")

        [cat_json] = Jason.decode!(response.body)

        %{external_id: cat_json["id"], url: cat_json["url"]}
    end
end