defmodule NeoKitten.VoteManager do
    import Ecto.Query
    alias NeoKitten.CatApiClient
    alias NeoKitten.Repo
    alias NeoKittenWeb.Cat
    alias NeoKittenWeb.Pair

    @new_pair_limit 10

    def create() do
        pair_count = Repo.aggregate(Pair, :count, :id)

        if (pair_count < @new_pair_limit || Enum.random([true, false])) do
            create_new_pair()
        else
            random_n = Enum.random(0..(pair_count-1))
            get_nth_pair(random_n)
        end
    end

    defp get_cat() do
        cat = CatApiClient.get()
        case Cat.create_or_return(cat) do
            {:new, cat} ->
                NeoKittenWeb.RankingChannel.broadcast_new_cat(cat)
                cat
            {:old, cat} -> cat
        end
    end

    defp get_different_cat(cat_id) do
        new_cat = get_cat()
        if (new_cat.id == cat_id) do
            get_different_cat(cat_id)
        else
            new_cat
        end
    end

    defp create_new_pair() do
        cat1 = get_cat()
        cat2 = get_different_cat(cat1.id)

        Pair.save_pair(cat1.id, cat2.id)

        {cat1, cat2}
    end

    defp get_nth_pair(n) do
        [pair] = Repo.all(from p in Pair, limit: 1, offset: ^n, preload: [:cat_one, :cat_two])

        if Enum.random([true, false]) do
            {pair.cat_one, pair.cat_two}
        else
            {pair.cat_two, pair.cat_one}
        end
    end
end