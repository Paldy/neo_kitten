defmodule NeoKitten.Repo do
  use Ecto.Repo,
    otp_app: :neo_kitten,
    adapter: Ecto.Adapters.Postgres
end
