defmodule NeoKittenWeb.RankingChannel do
  use Phoenix.Channel

  alias NeoKittenWeb.Cat
  alias NeoKittenWeb.Endpoint

  def join("ranking:live", _, socket) do
    {:ok, socket}
  end

  def broadcast_new_vote(%Cat{} = cat) do
    Endpoint.broadcast!("ranking:live", "new:vote", %{id: cat.id, url: cat.url})
  end

  def broadcast_new_cat(%Cat{} = cat) do
    Endpoint.broadcast!("ranking:live", "new:cat", %{id: cat.id, url: cat.url})
  end
end