defmodule NeoKittenWeb.Router do
  use NeoKittenWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", NeoKittenWeb do
    pipe_through :browser

    get "/", VoteController, :index
    post "/vote/w/:winner/l/:loser", VoteController, :vote

    get "/stats/", StatsController, :index

    get "/cutest", RankingController, :cutest
    get "/latest", RankingController, :latest

    get "/reset", ResetController, :index
    post "/reset", ResetController, :reset
  end

  # Other scopes may use custom stacks.
  # scope "/api", NeoKittenWeb do
  #   pipe_through :api
  # end
end
