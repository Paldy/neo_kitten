defmodule NeoKittenWeb.RankingController do
    use NeoKittenWeb, :controller

    import Ecto.Query

    alias NeoKitten.Repo
    alias NeoKittenWeb.Vote
    alias NeoKittenWeb.Cat

    def cutest(conn, _params) do
        cats_ranked = Cat.get_all_from_cutest()
            |> enhance_with_vote_percent()

        render(conn, "cutest.html", cats_ranked: cats_ranked)
    end


    def latest(conn, _params) do
        cats_ranked = Cat.get_all_from_latest()
            |> enhance_with_vote_percent()

        render(conn, "latest.html", cats_ranked: cats_ranked)
    end

    defp enhance_with_vote_percent(cats) do
        case Cat.get_cutest() do
            nil -> cats
            %{vote_count: 0} ->
                Enum.map(cats, fn cat -> Map.put(cat, :vote_to_percent, 0) end)
            %{vote_count: top_cat_votes} ->
                Enum.map(cats, fn cat -> Map.put(cat, :vote_to_percent, 100 * cat.vote_count / top_cat_votes) end)
        end
    end
end