defmodule NeoKittenWeb.ResetController do
    use NeoKittenWeb, :controller

    alias NeoKitten.Repo
    alias NeoKittenWeb.{Vote, Pair, Cat}

    def index(conn, _params) do
        render(conn, "index.html")
    end

    def reset(conn, _params) do
        Repo.delete_all(Vote)
        Repo.delete_all(Pair)
        Repo.delete_all(Cat)

        conn
        |> put_flash(:info, "All the data was deleted.")
        |> redirect(to: Routes.reset_path(conn, :index))
    end
end