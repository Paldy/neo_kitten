defmodule NeoKittenWeb.VoteController do
    use NeoKittenWeb, :controller

    alias NeoKitten.VoteManager
    alias NeoKitten.Repo
    alias NeoKittenWeb.{Pair, Vote, Cat}
  
    def index(conn, _params) do
      {cat1, cat2} = VoteManager.create()

      render(conn, "index.html", cat1: cat1, cat2: cat2)
    end

    # todo prevent revoting
    # - it could be done e.g. by storing the generated pair in session
    def vote(conn, %{"winner" => winner, "loser" => loser}) do
      pair = Pair.get_by_cats(winner, loser)

      {cat1, cat2} = VoteManager.create()

      if pair == nil do
        conn
        |> put_flash(:error, "Voting on non existent pair...")
        |> render("index.html", cat1: cat1, cat2: cat2)
      else
        changeset = Vote.changeset(%Vote{}, %{pair_id: pair.id, winner_id: winner})
        case Repo.insert(changeset) do
          {:ok, _} ->
            voted_cat = Repo.get(Cat, winner)
            NeoKittenWeb.RankingChannel.broadcast_new_vote(voted_cat)

            # todo this should be rather a redirect, but this way it is easier to test the app
            # i.e. you can F5 your vote to boost a cat (and test the live ranking)
            render(conn, "index.html", cat1: cat1, cat2: cat2)
          {:error, _} ->
            conn
            |> put_flash(:error, "Something went wrong with the vote...")
            |> render("index.html", cat1: cat1, cat2: cat2)
        end
      end
    end
  end
  