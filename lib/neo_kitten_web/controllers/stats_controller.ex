defmodule NeoKittenWeb.StatsController do
    use NeoKittenWeb, :controller
    alias NeoKitten.Repo
    alias NeoKittenWeb.Pair
    alias NeoKittenWeb.Vote
    
    require Ecto.Query

    def index(conn, _params) do
        pair_count = Repo.aggregate(Pair, :count, :id)

        query_vote = Ecto.Query.from v in Vote,
                    group_by: [v.pair_id, v.winner_id],
                    select: %{pair_id: v.pair_id, winner_id: v.winner_id, vote_count: count(1)}

        query_pair = Ecto.Query.from p in Pair,
                         left_join: c1 in subquery(query_vote), on: c1.pair_id == p.id and c1.winner_id == p.cat_one_id,
                         left_join: c2 in subquery(query_vote), on: c2.pair_id == p.id and c2.winner_id == p.cat_two_id,
                         select: %{pair: p, cat_one_votes: c1.vote_count, cat_two_votes: c2.vote_count},
                         preload: [:cat_one, :cat_two]

        all_pairs = Repo.all(query_pair)
                    |> enhance_pairs_with_percentage

        render(conn, "index.html", pair_count: pair_count, all_pairs: all_pairs)
    end

    defp enhance_pairs_with_percentage(all_pairs) do
        Enum.map(all_pairs, fn p ->
            p = p |> Map.update(:cat_one_votes, 0, fn v -> if v == nil, do: 0, else: v end) 
                |> Map.update(:cat_two_votes, 0, fn v -> if v == nil, do: 0, else: v end) 

            all_votes = p.cat_one_votes + p.cat_two_votes
            cat_one_percent = if all_votes == 0, do: 50, else: 20 + 60 * p.cat_one_votes / all_votes

            Map.put(p, :cat_one_percent, cat_one_percent)
        end)
    end
  end
  