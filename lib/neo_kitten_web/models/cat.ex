defmodule NeoKittenWeb.Cat do
    use Ecto.Schema

    import Ecto.Changeset
    import Ecto.Query

    alias NeoKitten.Repo
    alias NeoKittenWeb.Vote
    alias NeoKittenWeb.Cat

    schema "cats" do
        field :external_id, :string
        field :url, :string
        
        timestamps()
    end

    def changeset(%Cat{} = cat, attrs) do
        cat
        |> cast(attrs, [:external_id, :url])
        |> unique_constraint(:external_id)
        |> validate_required([:external_id, :url])
    end

    def create_or_return(ext_cat) do
        case Repo.get_by(Cat, external_id: ext_cat.external_id) do
            nil ->
                cat = Cat.changeset(%Cat{}, ext_cat)
                        |> Repo.insert!
                {:new, cat}
            cat -> {:old, cat}
        end
    end

    def get_cutest() do
        Ecto.Query.from(get_aggregated_votes_query(), order_by: [desc: fragment("vote_count")], limit: 1)
        |> Repo.one
    end

    def get_all_from_cutest() do
        Ecto.Query.from(get_aggregated_votes_query(), order_by: [desc: fragment("vote_count")])
        |> Repo.all
    end

    def get_all_from_latest() do
        Ecto.Query.from(get_aggregated_votes_query(), order_by: [desc_nulls_last: fragment("last_vote_time")])
        |> Repo.all
    end

    defp get_aggregated_votes_query() do
        Ecto.Query.from v in Vote,
                    right_join: c in assoc(v, :winner),
                    group_by: [c.id],
                    select: %{cat: c,
                            vote_count: fragment("count(?) as vote_count", v.id),
                            last_vote_time: fragment("max(?) as last_vote_time", v.inserted_at)}
    end
end