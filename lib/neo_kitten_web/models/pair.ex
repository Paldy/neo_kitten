defmodule NeoKittenWeb.Pair do
    use Ecto.Schema
    import Ecto.Changeset
    alias NeoKitten.Repo
    alias NeoKittenWeb.Cat
    alias NeoKittenWeb.Pair

    schema "pairs" do
        belongs_to :cat_one, Cat
        belongs_to :cat_two, Cat
        
        timestamps()
    end

    def changeset(%Pair{} = pair, attrs) do
        pair
        |> cast(attrs, [:cat_one_id, :cat_two_id])
        |> validate_required([:cat_one_id, :cat_two_id])
    end

    def save_pair(cat_one_id, cat_two_id) do
        {cat_one_id, cat_two_id} = normalize_pair(cat_one_id, cat_two_id)

        if (Repo.get_by(Pair, cat_one_id: cat_one_id, cat_two_id: cat_two_id) == nil) do
            Pair.changeset(%Pair{}, %{cat_one_id: cat_one_id, cat_two_id: cat_two_id})
            |> Repo.insert!
        end
    end

    def get_by_cats(cat_one_id, cat_two_id) do
        {cat_one_id, cat_two_id} = normalize_pair(cat_one_id, cat_two_id)

        Repo.get_by(Pair, cat_one_id: cat_one_id, cat_two_id: cat_two_id)
    end

    defp normalize_pair(cat_one_id, cat_two_id) do
        if (cat_one_id < cat_two_id) do
            {cat_one_id, cat_two_id}
        else
            {cat_two_id, cat_one_id}
        end
    end
end