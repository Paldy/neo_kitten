defmodule NeoKittenWeb.Vote do
    use Ecto.Schema
    import Ecto.Changeset
    alias NeoKitten.Repo
    alias NeoKittenWeb.Vote
    alias NeoKittenWeb.Pair
    alias NeoKittenWeb.Cat

    schema "votes" do
        belongs_to :pair, Pair
        belongs_to :winner, Cat
        
        timestamps()
    end

    def changeset(%Vote{} = cat, attrs) do
        cat
        |> cast(attrs, [:pair_id, :winner_id])
        |> validate_required([:pair_id, :winner_id])
    end
end