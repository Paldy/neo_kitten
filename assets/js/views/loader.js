// assets/js/view/loader.js

import MainView from './main';
import RankingCutestView from './page/ranking_cutest';
import RankingLatestView from './page/ranking_latest';

const views = {
    RankingCutestView,
    RankingLatestView
};

export default function loadView(viewName) {
    return views[viewName] || MainView;
}