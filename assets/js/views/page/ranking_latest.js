// assets/js/view/page/ranking_latest.js

import RankingView from './ranking';

export default class RankingLatestView extends RankingView {
    mount() {
        super.mount();
    }

    unmount() {
        super.unmount();
    }

    updateCatElementPosition(catElement) {
        catElement.parentElement.insertBefore(catElement, catElement.parentElement.childNodes[0] || null);
    }
}