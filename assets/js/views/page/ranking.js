// assets/js/view/page/ranking.js

import MainView from '../main';
import {Socket} from 'phoenix';

export default class RankingView extends MainView {
    mount() {
        super.mount();

        this.liveProcessing();
        this.findMostVotes();
    }

    liveProcessing() {
        let socket = new Socket("/socket");

        socket.connect();

        let channel = socket.channel("ranking:live", {});
        channel.join()
          .receive("ok", resp => { console.log("Joined successfully", resp) })
          .receive("error", resp => { console.log("Unable to join", resp) });

        let that = this;
        channel.on("new:vote", cat => {
            that.processNewVote(cat)
        })
        channel.on("new:cat", cat => {
            that.processNewCat(cat)
        })
    }

    findMostVotes() {
        this.mostVotes = Array.from(this.getRankingTBody().getElementsByTagName("tr"))
            .map(tr => tr.getElementsByClassName("cat-vote-in-table")[0])
            .map(td => Number(td.innerHTML))
            .reduce((max, cur) => Math.max(max, cur), 0);
    }

    unmount() {
        super.unmount();
    }

    processNewVote(cat) {
        let latestCatElement = document.getElementById(this.getCatElementId(cat.id));

        let latestVoteElement = document.getElementById(this.getCatVotesElementId(cat.id));
        let newVoteCount = Number(latestVoteElement.innerHTML) + 1;
        latestVoteElement.innerHTML = newVoteCount;
        latestCatElement.getElementsByClassName("cat-bar-in-table")[0].style.background = this.getBarBackground(newVoteCount);

        this.checkMostVotes(newVoteCount);

        this.updateCatElementPosition(latestCatElement);
    }

    updateCatElementPosition(catElement) {
    }

    processNewCat(cat) {
        let newTrCatElement = this.createTrCatElement(cat);

        this.getRankingTBody().appendChild(newTrCatElement);
    }

    createTrCatElement(cat) {
        let newCatElement = document.createElement("tr");
        newCatElement.setAttribute("id", this.getCatElementId(cat.id));

        newCatElement.appendChild(this.createTdImgElement(cat));
        newCatElement.appendChild(this.createTdCountElement(cat));
        newCatElement.appendChild(this.createTdBarElement());

        return newCatElement;
    }

    createTdImgElement(cat) {
        let tdImgElement = document.createElement("td");
        tdImgElement.className = "cat-in-table right-stats";
        tdImgElement.innerHTML = '<span class="helper"></span><img src="' + cat.url + '">';
        return tdImgElement;
    }

    createTdCountElement(cat) {
        let tdCountElement = document.createElement("td");
        tdCountElement.className = "cat-vote-in-table right-stats";
        tdCountElement.innerHTML = "0";
        tdCountElement.setAttribute("id", this.getCatVotesElementId(cat.id));
        return tdCountElement;
    }

    createTdBarElement() {
        let tdBarElement = document.createElement("td");
        tdBarElement.className = "cat-bar-in-table";
        tdBarElement.style.background = this.getBarBackground(0);
        return tdBarElement;
    }

    getRankingTBody() {
        return document.getElementById("ranking").getElementsByTagName("tbody")[0];
    }

    getBarBackground(votes) {
        let votesPercent = this.mostVotes == 0 ? 0 : 100 * votes / this.mostVotes;

        return `linear-gradient(to right, #111 ${votesPercent}%, #eee ${votesPercent}%)`;
    }

    getCatElementId(catId) {
        return "cat_" + catId;
    }

    getCatVotesElementId(catId) {
        return "cat_votes_" + catId;
    }

    checkMostVotes(votes) {
        if (votes > this.mostVotes) {
            this.mostVotes = votes;

            let that = this;
            this.getRankingTBody().querySelectorAll("tr")
                .forEach(tr => {
                    let votes = Number(tr.getElementsByClassName("cat-vote-in-table")[0].innerHTML);
                    tr.getElementsByClassName("cat-bar-in-table")[0].style.background = that.getBarBackground(votes);
                });
        }
    }
}