// assets/js/view/page/ranking_cutest.js

import RankingView from './ranking';

export default class RankingCutestView extends RankingView {
    mount() {
        super.mount();
    }

    unmount() {
        super.unmount();
    }

    updateCatElementPosition(catElement) {
        let catVoteCount = Number(catElement.getElementsByClassName("cat-vote-in-table")[0].innerHTML);

        let lessVotes = Array.from(catElement.parentElement.getElementsByTagName("tr"))
                .find(tr => Number(tr.getElementsByClassName("cat-vote-in-table")[0].innerHTML) < catVoteCount);

        catElement.parentElement.insertBefore(catElement, lessVotes || null);
    }
}