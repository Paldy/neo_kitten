defmodule NeoKitten.Repo.Migrations.AddCatsAndVotesTable do
  use Ecto.Migration

  def change do
    create table(:cats) do
      add :external_id, :string, null: false
      add :url, :string, null: false

      timestamps()
    end

    create unique_index(:cats, [:external_id])

    create table(:pairs) do
      add :cat_one_id, references(:cats), null: false
      add :cat_two_id, references(:cats), null: false

      timestamps()
    end

    create table(:votes) do
      add :pair_id, references(:pairs), null: false
      add :winner_id, references(:cats), null: false

      timestamps()
    end
  end
end
